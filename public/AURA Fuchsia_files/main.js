$(document).ready(function () {
    "use strict";

    $(".header-area").sticky();

    /*-------------------------------------------------------------------------------
    banner Slider 
	-------------------------------------------------------------------------------*/
    $('.banner-area').owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        autoplay: false,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        autoplaySpeed: 1000,
        nav: false,
    });

    /*-------------------------------------------------------------------------------
    toggle items 
	-------------------------------------------------------------------------------*/
    $(".arrow-down").click(function () {
        $(".collapse-items").slideToggle();
    });
    $(".arrow-down").click(function () {
        $(".arrow-down").hide();
        $(".arrow-up").show();
    });

    $(".arrow-up").click(function () {
        $(".collapse-items").slideUp();
    });
    $(".arrow-up").click(function () {
        $(".arrow-up").hide();
        $(".arrow-down").show();
    });


});