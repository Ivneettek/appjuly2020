import * as actionTypes from "../actions/actionTypes";

const initialState = {
  ajaxCallsInProgress: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.BEGIN_AJAX_CALL: {
      return { ...state, ajaxCallsInProgress: state.ajaxCallsInProgress + 1 };
    }
    case actionTypes.AJAX_CALL_ERROR: {
      return { ...state, ajaxCallsInProgress: state.ajaxCallsInProgress - 1 };
    }
    case actionTypes.END_AJAX_CALL: {
      return { ...state, ajaxCallsInProgress: state.ajaxCallsInProgress - 1 };
    }
    default:
      return state;
  }
};
