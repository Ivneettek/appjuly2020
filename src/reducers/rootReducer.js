import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import ajaxStatusReducer from "./ajaxStatusReducer";
import locationReducer from "./locationReducer";

const rootReducer = combineReducers({
  ajaxStatusReducer,
  location: locationReducer,
  form: formReducer
});

export default rootReducer;
