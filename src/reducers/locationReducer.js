import * as locationActionTypes from "actions/locationActionTypes";
import * as actionTypes from "actions/actionTypes";

const initialState = {
  fetchingLocation: false,
  location: []
};

const locationReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.BEGIN_AJAX_CALL:
      return {
        ...state,
        fetchingLocation: true
      };
    case locationActionTypes.FETCH_LOCATION:
      return {
        ...state,
        fetchingLocation: false,
        location: action.payload
      };

    default:
      return state;
  }
};

export default locationReducer;
