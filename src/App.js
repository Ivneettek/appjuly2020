import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";
import location from "./views/location";
import { connect } from "react-redux";

class App extends React.Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={location} />
        <Route path="/trace" component={location} />
      </div>
    );
  }
}

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  match: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    loading: state.ajaxCallsInProgress > 0
  };
}

export default connect(mapStateToProps)(App);
