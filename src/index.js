import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router } from "react-router-dom";

import "assets/AURA Fuchsia_files/main.css";

import { Provider } from "react-redux";
import store from "store/store";
import App from "./App";

//ReactDOM.render(<SourceTrace />, document.getElementById("sourcetrace"));

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById("location")
);
