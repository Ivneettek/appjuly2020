import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchLocation } from "actions/locationActions";
import queryString from "query-string";

class location extends Component {
  componentDidMount() {
    const values = queryString.parse(this.props.location.search);
    console.log(values.id);
    this.props.fetchLocation(values.id);
  }

  render() {
    return (
      <div>
        {this.props.location.address_traceability
          ? this.props.location.address_traceability.map(address => (
              <div className="single-location-item">
                <img src="./AURA Fuchsia_files/location1.png" alt="" />
                <p>{address.full_address}</p>
              </div>
            ))
          : null}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    ajaxCallsInProgress: state.ajaxStatusReducer.ajaxCallsInProgress,
    location: state.location.location
  };
}

const mapDispatchToProps = dispatch => ({
  fetchLocation: id => dispatch(fetchLocation(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(location);
