import axios from "axios";
import * as types from "./actionTypes";
import * as locationActionTypes from "actions/locationActionTypes";
import { beginAjaxCall, endAjaxCall, ajaxCallError } from "./ajaxStatusActions";

export function onFetchLocationSuccess(data) {
  return { type: locationActionTypes.FETCH_LOCATION, data };
}

export function fetchLocation(id) {
  return function(dispatch) {
    //console.log("*****************************");
    //console.log(id);
    dispatch(beginAjaxCall());

    return axios
      .get("http://localhost:5000/trace/" + id)
      .then(response => {
        if (response.status == 200) {
          dispatch({
            type: locationActionTypes.FETCH_LOCATION,
            payload: response.data
          });
          dispatch(endAjaxCall());
          //dispatch(onFetchTracesSuccess(response));
        }
        console.log(response.data);
        //const products = response.data;
      })
      .catch(error => {
        throw error;
      });
  };
}
